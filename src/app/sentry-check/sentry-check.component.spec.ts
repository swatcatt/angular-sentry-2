import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SentryCheckComponent } from './sentry-check.component';

describe('SentryCheckComponent', () => {
  let component: SentryCheckComponent;
  let fixture: ComponentFixture<SentryCheckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SentryCheckComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SentryCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
